#!/usr/bin/env python
# encoding: utf-8
"""
1. Cat vs. Dog (Difficulty Level: Hard)
https://labs.spotify.com/puzzles/
"""

def solution(votes):
    """
    solution function
    """
    number_happy_voters = 0
    if not votes:
        return number_happy_voters
    while True:
        pet_min_score = sorted(votes.items(), key=lambda x: (x[1]["score"]))[0]
        if pet_min_score[1]["score"] <= 0:
            for pet_name in pet_min_score[1]["against"]:
                pet = votes[pet_name]
                pet["score"] += 1
            del votes[pet_min_score[0]]
        else:
            break
    for pet_name, pet_info in votes.iteritems():
        number_happy_voters += pet_info["score"]
    return number_happy_voters

def main():
    """
    main function
    """
    num_testcases = int(raw_input().strip())
    for _ in xrange(num_testcases):
        testcase = dict()
        _, _, voters = map(int, raw_input().strip().split(' '))
        for _ in xrange(voters):
            vote1, vote2 = raw_input().strip().split(' ')
            if not vote1 in testcase:
                testcase[vote1] = dict()
                testcase[vote1]["score"] = 0
                testcase[vote1]["against"] = []
            testcase[vote1]["score"] += 1
            testcase[vote1]["against"].append(vote2)
            if not vote2 in testcase:
                testcase[vote2] = dict()
                testcase[vote2]["score"] = 0
                testcase[vote2]["against"] = []
            testcase[vote2]["score"] -= 1
        print solution(testcase)

if __name__ == '__main__':
    main()
