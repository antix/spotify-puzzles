#!/usr/bin/env python
# encoding: utf-8

def solution(votes):
    """
    >>> votes = {'C1': {'against': ['D1'], 'score': -1},
    ...  'C2': {'against': ['D2'], 'score': -1},
    ...  'C3': {'against': ['D2'], 'score': 0},
    ...  'D1': {'against': ['C1'], 'score': 0},
    ...  'D2': {'against': ['C2'], 'score': -1},
    ...  'D3': {'against': ['C1'], 'score': 1},
    ...  'D4': {'against': ['C3'], 'score': 1},
    ...  'D5': {'against': ['C2'], 'score': 1}}
    >>> solution(votes)
    5
    """
    number_happy_voters = 0
    if not votes:
        return number_happy_voters
    while True:
        pet_min_score = sorted(votes.items(), key=lambda x: (x[1]["score"]))[0]
        if pet_min_score[1]["score"] <= 0:
            for pet_name in pet_min_score[1]["against"]:
                pet = votes[pet_name]
                pet["score"] += 1
            del votes[pet_min_score[0]]
            print "removing", pet_min_score[0]
        else:
            break
    for pet_name, pet_info in votes.iteritems():
        number_happy_voters += pet_info["score"]
        print "Pet", pet_name, "give us", pet_info["score"]
    return number_happy_voters

def main():
    num_testcases = int(raw_input().strip())
    for _ in xrange(num_testcases):
        testcase = dict()
        _, _, voters = map(int, raw_input().strip().split(' '))
        for _ in xrange(voters):
            vote1, vote2 = raw_input().strip().split(' ')
            if not vote1 in testcase:
                testcase[vote1] = dict()
                testcase[vote1]["score"] = 0
                testcase[vote1]["against"] = []
            testcase[vote1]["score"] += 1
            testcase[vote1]["against"].append(vote2)
            if not vote2 in testcase:
                testcase[vote2] = dict()
                testcase[vote2]["score"] = 0
                testcase[vote2]["against"] = []
            testcase[vote2]["score"] -= 1
        print sorted(testcase.items(), key=lambda x: (x[0]))
        print solution(testcase)

if __name__ == '__main__':
    main()
    import doctest
    doctest.testmod()
