#!/usr/bin/env python
# encoding: utf-8
"""
3. Reversed Binary Numbers (Difficulty Level: Easy)
https://labs.spotify.com/puzzles/
"""

def solution2(number):
    """
    >>> solution2(13)
    11
    >>> solution2(47)
    61
    """
    print int("".join(list(reversed(list(bin(number)[2:])))), 2)


def solution(number):
    """
    >>> solution(13)
    11
    >>> solution(47)
    61
    """
    result = 0
    while number > 0:
        result <<= 1 # left shift result
        #print bin(result)
        result |= number & 0b1 # get last bit of number and set it to result
        #print bin(result)
        number >>= 1 # right shift number
        #print bin(number)
        #print "-"*10
    return result

def main():
    """
    main function
    """
    number = int(raw_input().strip())
    print solution(number)

if __name__ == '__main__':
    main()
    import doctest
    doctest.testmod()
