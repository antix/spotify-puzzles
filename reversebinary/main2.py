#!/usr/bin/env python
# encoding: utf-8

def solution(number):
    return int("".join(list(reversed(list(bin(number)[2:])))), 2)

def main():
    number = int(raw_input().strip())
    print solution(number)

if __name__ == '__main__':
    main()
