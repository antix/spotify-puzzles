#!/usr/bin/env python
# encoding: utf-8

def solution(number):
    result = 0
    while number > 0:
        result <<= 1 # left shift result
        result |= number & 0b1 # get last bit of number and set it to result
        number >>= 1 # right shift number
    return result

def main():
    number = int(raw_input().strip())
    print solution(number)

if __name__ == '__main__':
    main()
