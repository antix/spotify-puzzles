#!/usr/bin/env python
# encoding: utf-8
"""
3. Reversed Binary Numbers (Difficulty Level: Easy)
https://labs.spotify.com/puzzles/
"""

from collections import defaultdict

def main():
    """
    main function
    """
    songs = defaultdict(dict) # replace to list of tupples
    number_songs, number_best_songs = map(int, raw_input().strip().split(' '))
    for i in xrange(1, number_songs+1):
        line = raw_input().strip().split(' ')
        rate = int(line[0])
        song_name = line[1]
        songs[song_name]["rate"] = rate * i
        songs[song_name]["index"] = i
    for song in sorted(songs.items(), key=lambda x: (x[1]["rate"], -x[1]["index"]), reverse=True)[:number_best_songs]:
        print song[0]

if __name__ == '__main__':
    main()
