#!/usr/bin/env python
# encoding: utf-8
"""
3. Reversed Binary Numbers (Difficulty Level: Easy)
https://labs.spotify.com/puzzles/
"""

def main():
    """
    main function
    """
    songs = list()
    number_songs, number_best_songs = [int(x) for x in raw_input().strip().split(' ')]
    for i in xrange(1, number_songs+1):
        line = raw_input().strip().split(' ')
        rate = int(line[0])
        song_name = line[1]
        songs.append((song_name, rate * i, i))
    print "\n".join([song[0] for song in sorted(songs, key=lambda x: (x[1], -x[2]), reverse=True)[:number_best_songs]])

if __name__ == '__main__':
    main()
